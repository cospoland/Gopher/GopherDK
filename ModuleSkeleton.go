package GopherDK

type Module struct {
	Name          string
	Version       string
	Author        string
	Configuration map[string]string
	Commands      map[string]interface{}
	Methods       ModuleMethodSkeleton
	Extra         map[string]interface{}
}

type ModuleMethodSkeleton interface {
	Start() bool
	Send(Context MessageContext, Text string) bool
	Mention(User string) string
}
