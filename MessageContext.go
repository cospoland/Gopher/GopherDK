package GopherDK

type MessageContext struct {
	Module     Module
	Provider   Module
	Command    string
	Parameters []string
	Extra      map[string]interface{}
}
